require('../css/app.css');
const $ = require('jquery');
const axios = require('axios');
const {get, isNull} = require('lodash');

const getShortUrl = async ({url}) => {
    try {
        const response = await axios.post('http://127.0.0.1:8000/short/url', {
            url,
        });
        console.log(response);
        return get(response, 'data.result.short_url', null);
    } catch (err) {
        console.log(err);
    }
};

const setShortUrlToSpan = ({shortUrl}) => {
    try {
        $('.short-url').text(shortUrl);
    } catch (err) {
        console.log(err);
    }
};

const getUrlFromInput = () => {
    try {
        return $('.url').val();
    } catch (err) {
        console.log(err);
    }
};

$('.get-short-url').click(() => {
    const url = getUrlFromInput();
    getShortUrl({url})
        .then((shortUrl) => {
            setShortUrlToSpan({shortUrl});
        })
        .catch((error) => {
            console.log(error);
        });
});
