<?php

namespace App\Controller;

use App\Service\ShortUrlService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/{shortUrl}", name="main")
     * @param Request $request
     * @param ShortUrlService $shortUrlService
     * @param string $shortUrl
     * @return Response
     */
    public function index(Request $request, ShortUrlService $shortUrlService, string $shortUrl = '/')
    {
        if ($request->getRequestUri() === '/' ||
            !$shortUrlService->isExistUrlByShortUrl($shortUrl)) {
            return $this->render('base.html.twig');
        }

        return $this->redirect($shortUrlService->getUrlByShortUrl($shortUrl));
    }

    /**
     * @Route("/short/url", name="get_short_url", methods="POST")
     * @param Request $request
     * @param ShortUrlService $dbService
     * @return JsonResponse
     */
    public function getShortUrl(Request $request, ShortUrlService $dbService)
    {
        $requestData = json_decode($request->getContent(), true);
        $shortUrl = $dbService->createShortUrl($requestData['url']);
        return $this->json([
            'success' => true,
            'result' => [
                'short_url' => $shortUrl,
            ]
        ]);
    }
}
