<?php


namespace App\Helper;

use Redis;

class KeyValueDbHelper
{
    private const TTL = 86400;
    /**
     * @var Redis
     */
    private $redis;

    public function __construct()
    {
        $this->redis = new Redis();
        $this->dbConnect();
    }

    /**
     * Добавляет объект по ключу
     *
     * @param string $key
     * @param string $value
     * @return bool|string
     */
    public function setByKey(string $key, string $value)
    {
        return $this->redis->setex($key, self::TTL, $value);
    }

    /**
     * Возвращает объект по ключу
     *
     * @param string $key
     * @return bool|string
     */
    public function getByKey(string $key)
    {
        return $this->redis->get($key);
    }

    /**
     * Проверяет, существует ли ключ
     *
     * @param string $hash
     * @return int
     */
    public function isExistKey(string $hash)
    {
        return $this->redis->exists($hash);
    }

    private function dbConnect()
    {
        $this->redis->connect('localhost');
    }
}
