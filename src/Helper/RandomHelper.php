<?php

namespace App\Helper;

class RandomHelper
{
    public function generateUniqueId()
    {
        return uniqid();
    }
}
