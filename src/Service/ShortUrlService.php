<?php

namespace App\Service;

use App\Helper\RandomHelper;
use App\Helper\KeyValueDbHelper;

class ShortUrlService
{
    private const ADDRESS = 'https://jns.by/';

    /**
     * @var ShortUrlService
     */
    private $dbHelper;

    /**
     * @var RandomHelper
     */
    private $randomHelper;

    /**
     * ShortUrlService constructor.
     */
    public function __construct()
    {
        $this->dbHelper = new KeyValueDbHelper();
        $this->randomHelper = new RandomHelper();
    }

    /**
     * Создаёт сокращённый URL-адрес
     *
     * @param string $url
     * @return string
     */
    public function createShortUrl(string $url)
    {
        $uniqueId = $this->randomHelper->generateUniqueId();
        $this->dbHelper->setByKey($uniqueId, $url);
        return self::ADDRESS . $uniqueId;
    }

    /**
     * Возвращает ранее созданный сокращённый URL-адрес
     *
     * @param string $shortHash
     * @return string
     */
    public function getUrlByShortUrl(string $shortHash)
    {
        return $this->dbHelper->getByKey($shortHash);
    }

    /**
     * Проверяет, есть ли сокращённый URL по переданному запросу
     *
     * @param string $shortHash
     * @return string
     */
    public function isExistUrlByShortUrl(string $shortHash)
    {
        return $this->dbHelper->isExistKey($shortHash);
    }
}
