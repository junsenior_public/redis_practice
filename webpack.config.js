var Encore = require('@symfony/webpack-encore');

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('app-js', ['./assets/js/uikit.js','./assets/js/uikit-icons.js', './assets/js/app.js'])
    .addEntry('app-css', ['./assets/css/uikit.css','./assets/css/uikit-rtl.css', './assets/js/app.js'])
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())
    .configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })
    .autoProvidejQuery()
;

module.exports = Encore.getWebpackConfig();
